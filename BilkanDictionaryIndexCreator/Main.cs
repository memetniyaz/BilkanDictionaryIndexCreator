﻿using Nest;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace BilkanDictionaryIndexCreator
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void Start_Click(object sender, EventArgs e)
        {
            string textFilepath = path.Text;
            if (!string.IsNullOrEmpty(textFilepath.Trim()))
            {
                
                createIndex(textFilepath);
            }
            else {
                MessageBox.Show("请选择词典文件。");
                Status.Text = "请选择词典文件！";
                open.Enabled = true;
                Start.Enabled = false;
            }
        }

        public void createIndex(string textFilepath)
        {
            StreamReader sr = new StreamReader(textFilepath);
            List<Word> words = new List<Word>();
            while (!sr.EndOfStream)
            {
                string str = sr.ReadLine();
                string[] thisLine = Regex.Split(str, "\t", RegexOptions.IgnoreCase);
                if (thisLine.Length >= 2)
                {
                    Word word1 = new Word()
                    {
                        key = thisLine[0],
                        value = thisLine[1],
                        transcription = thisLine[2]
                        
                };
                    words.Add(word1);
                }
            }
            cnug Dictionary = new cnug()
            {
                Words = words.ToArray()
            };

            ElasticClient client = getElasticClient("dictionaries");
            IResponse response = createIndexItem(client, Dictionary);
            if (response.IsValid)
            {
                MessageBox.Show("完成 ！");
                Status.Text = "任务完成！";
                open.Enabled = true;
                Start.Enabled = false;
            }
            else {
                MessageBox.Show("出现错误 ！");
                Status.Text = "失败！";
                open.Enabled = false;
                Start.Enabled = true;
            }
        }


        public ElasticClient getElasticClient(string indexName)
        {
            Uri serverUri = new Uri("http://localhost:9200");
            ConnectionSettings settings = new ConnectionSettings(serverUri, indexName);
            ElasticClient client = new ElasticClient(settings);
            return client;
        }

        public IResponse createIndexItem(ElasticClient client, cnug Dictionary)
        {
            IResponse response = client.Index(Dictionary);
            return response;
        }

        private void open_Click(object sender, EventArgs e)
        {
            OpenFileDialog m = new OpenFileDialog();
            m.Filter = "Window 文本文件(*.txt)|*.txt";

            if (m.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string textFilepath = m.FileName;
                    path.Text = textFilepath;
                    open.Enabled = false;
                    Start.Enabled = true;
                }

                catch
                {
                    MessageBox.Show("出现错误！", "警告", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void Main_Load(object sender, EventArgs e)
        {
            Status.Text =string.Empty;
            path.Text = "请选择词典文件 !";
            Start.Enabled = false;
        }
    }



    public class cnug
    {
        public Word[] Words { get; set; }
    }

    public class Word
    {
        public string key { get; set; }
        public string transcription { get; set; }
        public string value { get; set; }
    }

}
